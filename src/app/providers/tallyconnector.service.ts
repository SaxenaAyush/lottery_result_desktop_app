import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { map } from "rxjs/operators";

@Injectable()
export class TallyConnectorService {

    constructor(private http: Http){}
    connectToApi(url:string,data:any){
        return this.http.post(url,data).pipe(map((response) =>response.json()));        
    }
}