import 'reflect-metadata';
import '../polyfills';
import { Component } from '@angular/core';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalComponent } from './modal.component';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ElectronService } from './providers/electron.service';
import { TallyConnectorService } from './providers/tallyconnector.service';
import { WebviewDirective } from './directives/webview.directive';
import { AppComponent } from './app.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { SharedModule } from './common/sharaed/sharaed.module';
import { HttpModule } from '@angular/http';
import { NgxSelectModule } from 'ngx-select-ex';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QRCodeModule } from 'angularx-qrcode';
import { DatePipe } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { TermsComponent } from './components/login/terms-modal.component';
import { from } from 'rxjs';
const { remote } = require('electron');
import { NgxBarcodeModule } from 'ngx-barcode';
import { ShowErrorsComponent } from './validator/show-error';
import {NgxChildProcessModule} from 'ngx-childprocess';
import {NgxFsModule} from 'ngx-fs';
import {HotkeyModule} from 'angular2-hotkeys';
import { ResultComponent } from './components/distributor/result/result.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



@NgModule({

  declarations: [
    AppComponent,
    ShowErrorsComponent,
    WebviewDirective,
    ModalComponent,
    TermsComponent,   

    HomeLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    ResultComponent,
    ShowErrorsComponent,
 
  ],
  imports: [
    SharedModule.forRoot(),
    BrowserModule,
    FormsModule,    
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    NgxSelectModule,
    NgxQRCodeModule,
    QRCodeModule,
    NgxBarcodeModule,
    NgxChildProcessModule,
    NgxFsModule,
    HotkeyModule.forRoot(),
    NgxDaterangepickerMd.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService, TallyConnectorService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}