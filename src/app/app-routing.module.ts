import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './components/guard/auth.guard';
import { ResultComponent } from './components/distributor/result/result.component';
const routes: Routes = [

  {
    path: 'distributor',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
     
      
      {
        path: 'result',
        component: ResultComponent
      },
     
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },


    ]
  },
 


  
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}