import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-home-layout',
  template: `

  <div>
  <router-outlet></router-outlet> 
  </div>


  `,
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {

   
  }
//   <div class="row" style="margin:0;">
//   <div class="col-md-1" style="width:72px;flex: none;"> <app-side-nav></app-side-nav> </div>
//   <div class="col-md-11">
//    <router-outlet></router-outlet>
// </div>
//   </div>
   

//   <html>
//   <head>
//       <meta charset="utf-8">
//       <meta name="viewport" content="width=device-width, initial-scale=1.0">
//       <meta http-equiv="X-UA-Compatible" content="IE=edge">

//       <title>Collapsible sidebar using Bootstrap 3</title>

//        <!-- Bootstrap CSS CDN -->
//       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
//       <!-- Our Custom CSS -->
      
//   </head>
//   <body>



//       <div class="wrapper">
//           <!-- Sidebar Holder -->
//           <nav id="sidebar">
//               <div class="sidebar-header">
//                   <h3>Bhagyashree</h3>
//                   <strong>BL</strong>
//               </div>

//               <ul class="list-unstyled components">
//               <li class="nav-item" *ngIf="id == 4">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/booking"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/booking.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Booking"><h4>Booking</h4>
//               </a>
              
//             </li>
//             <li class="nav-item" *ngIf="id == 4">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/claim-transaction"
//                 href="javascript:void(0)">
//                <i> <img src="assets/icons/refund.png" class="app-icon" width="40" height="40" data-toggle="tooltip"
//                   data-placement="top" title="Claim-Transaction"></i><h4>Booking</h4>
//               </a>
//             </li>
//             <li class="nav-item">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/wallet-Details"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/wallet .png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Wallet-Details"><h4>Booking</h4>
//               </a>
//             </li>
//             <!--*ngIf="this.userForm.value.usrName == 'admin'"-->
//             <li class="nav-item" *ngIf="id == 1">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/distributor/view-client"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/avatar.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="View-Client"><h4>Booking</h4>
//               </a>
//             </li>
//             <li class="nav-item" *ngIf="id == 4">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/add-details"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/file.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Add-Details">
//               </a>
//             </li>
  
//             <li class="nav-item">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/result-view"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/experiment-results.png" class="app-icon" width="30" height="30"
//                   data-toggle="tooltip" data-placement="top" title="Result-view">
//               </a>
//             </li>
//             <li class="nav-item">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/result-report"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/growth.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Result-Report">
//               </a>
//             </li>
//             <li class="nav-item" *ngIf="id == 4">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/user-profile"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/user.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Profile">
//               </a>
//             </li>
//             <li class="nav-item" *ngIf="id == 1">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/distributor/admin-profile"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/user.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Profile">
//               </a>
//             </li>
  
//             <li class="nav-item">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/client/setting"
//                 href="javascript:void(0)">
//                 <img src="assets/icons/settings-gears.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="Settings">
//               </a>
//             </li>
  
//             <li class="nav-item">
//               <a class="nav-link" routerLinkActive="active-link active" routerLink="/login" href="javascript:void(0)">
//                 <img src="assets/icons/logout.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                   data-placement="top" title="logout">
//               </a>
//             </li>
//             <li class="nav-item">
//                 <a class="nav-link" routerLinkActive="active-link active" routerLink="/collapsing" href="javascript:void(0)">
//                   <img src="assets/icons/logout.png" class="app-icon" width="30" height="30" data-toggle="tooltip"
//                     data-placement="top" title="logout">
//                 </a>
//               </li>
                
//               </ul>

//               </nav>

//           <!-- Page Content Holder -->
//           <div id="content">

//               <nav class=" navbar-default">
//                   <div class="container-fluid">

//                       <div class="navbar-header">
//                           <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
//                               <i class="glyphicon glyphicon-align-left"></i>
//                               <span></span>
//                           </button>
//                       </div>     
//                   </div>
//               </nav>  
              
//               <p><router-outlet></router-outlet></p>
             
//            </div>
//       </div>
//   </body>
// </html>
}
