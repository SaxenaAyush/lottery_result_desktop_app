import { environment } from '../../environments/environment';

export class ConstantsHelper {


  public static readonly getAPIUrl = {

    addClient: () => `${environment.baseUrl}/lottery/signup`,
    userLogin: () => `${environment.baseUrl}/lottery/alldetailsignin`,
    booking: () => `${environment.baseUrl}/lottery/book-ticket`,
    kycDetails: () => `${environment.baseUrl}/lottery/add-kyc-detail`,
    personalDetail: () => `${environment.baseUrl}/lottery/add-personal-detail`,
    bankDetails: () => `${environment.baseUrl}/lottery/add-bank-detail`,
    getRoleList: () => `${environment.baseUrl}/lottery/get-roles`,
    getPassbookDetails: () => `${environment.baseUrl}/wallet/users/1/passbook`,
    slotlist: () => `${environment.baseUrl}/lottery/get-slot-list`,
    priceList: () => `${environment.baseUrl}/lottery/get-ticket-price`,
    //  userDetails: (id: number) => `${environment.baseUrl}/lottery/get-employeeById/`+id,
    //  getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}/lottery/get-employeeById/`+bookingDate+'/' +id,
    viewResults: () => `${environment.baseUrl}/lottery/get-result`,
    clientDetails: () => `${environment.baseUrl}/lottery/get-employee`,
    getUserByCreator: () => `${environment.baseUrl}/lottery/getUserListByCreator`,
    getAllUserListByCreator: () => `${environment.baseUrl}/lottery/getAllUserListByCreator`,
    deleteClient: () => `${environment.baseUrl}/lottery/getChangeStatusOfUser`,
    reset: () => `${environment.baseUrl}/lottery/resetPassword`,
    changePassword: () => `${environment.baseUrl}/lottery/change-password`,
    viewClient: () => `${environment.baseUrl}/lottery/userDetailSignin`,
    deductBalance: () => `${environment.baseUrl}/lottery/deduct-amount-bycreator`,
    // licenseRenewal: () => `${environment.baseUrl}/lottery/licence-renewal`,
    changeParent: () => `${environment.baseUrl}/lottery/change-parent`,
    settledCliam: () => `${environment.baseUrl}/lottery/settled-claim`,
    searchByNumber: () => `${environment.baseUrl}/lottery/find-my-number`,
    farwordClaim: () => `${environment.baseUrl}/lottery/request-claim`,
    viewRequestClaim: () => `${environment.baseUrl}/lottery/view-requested-claim`,
    viewDispatchClaim: () => `${environment.baseUrl}/lottery/view-dispatch-claim`,
    viewAllTransactionClaim: () => `${environment.baseUrl}/lottery/view-claimed-history`,
    viewWalletBalance: () => `${environment.baseUrl}/lottery/view-wallet-balance`,
    viewCommissionBalance: () => `${environment.baseUrl}/lottery/get-bonus-balance`,   
    viewWalletRequest: () => `${environment.baseUrl}/lottery/view-wallet-request`,
    myAllEmployee: () => `${environment.baseUrl}/lottery/my-all-employee`,
    viewCredit: () => `${environment.baseUrl}/lottery/view-debit-credit-datewise`,
    viewWalletBonus: () => `${environment.baseUrl}/lottery/view-wallet-claim-bonus-datewise`,
     approv: () => `${environment.baseUrl}/lottery/approve-wallet-request`,
     requestWalletAmount: () => `${environment.baseUrl}/lottery/request-wallet-amount`,
     allTransaction: () => `${environment.baseUrl}/lottery/view-wallet-transaction-datewise`,
     getAmount: () => `${environment.baseUrl}/lottery/view-wallet-balance`,
     getAllResult: () => `${environment.baseUrl}/lottery/get-result-bydate`,
     commission: () => `${environment.baseUrl}/lottery/view-wallet-commission`,
     saveCommission: () => `${environment.baseUrl}/lottery/save-single-commission`,
     updateEmployee: () => `${environment.baseUrl}/lottery/update-employee`,
     ticketHistory: () => `${environment.baseUrl}/lottery/ticket-by-user`,
     ticketHistoryByDate: () => `${environment.baseUrl}/lottery/ticket-by-userIdAndDate`,
     cancelHistoryByDate: () => `${environment.baseUrl}/lottery/cancle-history-bydate`,
     bookingvsClaim: () => `${environment.baseUrl}/lottery/get_ticketbooking_claim_bydate`,
     allUserCreatedBy: () => `${environment.baseUrl}/lottery/allUserCreatedBy`, 
     allUserCreatedById: () => `${environment.baseUrl}/lottery/get_ticketbooking_claim_bydate_byRetailerID`,
     walletHistory: () => `${environment.baseUrl}/lottery/allUserRechargeDetails`,
     retailerWalletHistory: () => `${environment.baseUrl}/lottery/RetailerWalletDetails`,
     slotDetails: () => `${environment.baseUrl}/lottery/get_ticketbooking_claim_bydate_byRetailerID_slotID`,
     reportDatewise: () => `${environment.baseUrl}/lottery/retailer-report-datewise`,
     toFromDateReport: () => `${environment.baseUrl}/lottery/get_ticketbooking_claim_byTwodate_byRetailerID`,
     loginId: () => `${environment.baseUrl}/lottery/alldetailsuser`,
     bookingId: () => `${environment.baseUrl}/lottery/ticket-by-bookingid`,
     ticketCancle: () => `${environment.baseUrl}/lottery/cancle-ticket`,
     transferAmount: () => `${environment.baseUrl}/lottery/transfer-amount`,
     retailerRequested: () => `${environment.baseUrl}/lottery/view-my-wallet-request-datewise`,
     Transfer: () => `${environment.baseUrl}/lottery/approve-direct-wallet-request`,
     getLicense: () => `${environment.baseUrl}/lottery/get-active-licence`,
     licenceRenewal:() => `${environment.baseUrl}/lottery/licence-renewal`,
     support:() => `${environment.baseUrl}/lottery/supports`,
     resetMachine:() => `${environment.baseUrl}/lottery/reset-machinekey`,
     dailyReport:() => `${environment.baseUrl}/lottery/retailer-report`,
     inactive:() => `${environment.baseUrl}/lottery/active-inactive-user`,
     //  userDetails: (id: number) => `${environment.baseUrl}/lottery/get-employeeById/`+id,
     //  getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}/lottery/get-employeeById/`+bookingDate+'/' +id,
    
    http://localhost:8080/find-my-number

    {
      "id": 160
    }
  };
  
}
