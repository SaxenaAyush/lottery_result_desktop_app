import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SweetAlertService} from './../../common/sharaed/sweetalert2.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TwoDecimalFormatNumberPipe } from '../../pipes/two-decimal-format-number.pipe';
// import { PdfmakeModule } from '../../../../node_modules/ng-pdf-make';
// import { Observable } from 'rxjs';


@NgModule({
  imports: [CommonModule],
  declarations: [ TwoDecimalFormatNumberPipe],
  providers: [SweetAlertService],
  exports: [ NgbModule, TwoDecimalFormatNumberPipe
    ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [SweetAlertService]
    };
  }
}
